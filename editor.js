
/**
 * @file
 * This file handles the interaction with the plain editor.
 *
 * Copyright OpenCraft |  http://www.open-craft.com
 * Maintainer: msameer@open-craft.com
 *
 * This software is distributed under the GNU General Public License.
 * Read the entire license text here: http://www.gnu.org/licenses/gpl.html
 */

var elem="edit-map";

function on_click_event()
{
  window.opener.document.getElementById(elem).value = document.getElementById('gmap-macrotext').value;
  window.close();
}
