
/**
 * @file
 * This file mainly has the functions needed to interact with tinymce.
 *
 * Copyright OpenCraft |  http://www.open-craft.com
 * Maintainer: msameer@open-craft.com
 *
 * This software is distributed under the GNU General Public License.
 * Read the entire license text here: http://www.gnu.org/licenses/gpl.html
 */

function map_get_width(t)
{
  var arr=t.split('|');
  for (x in arr)
    {
      var t2 = arr[x];
      var arr2 = t2.split('=');
      if (arr2[0] == 'width')
	{
	  return arr2[1];
	}
    }
}

function map_get_height(t)
{
  var arr=t.split('|');
  for (x in arr)
    {
      var t2 = arr[x];
      var arr2 = t2.split('=');
      if (arr2[0] == 'height')
	{
	  return arr2[1];
	}
    }
}

function map_done()
{
  window.opener.location.reload();
  window.close();
}

function node_changed(u)
{
  var nid = document.getElementById('edit-node').value;
  map_frame = parent.frames['map_frame'];
  //document.getElementById('map_frame');
  //alert(map_frame.style.display);
  //alert(nid);
  if (nid == 0)
    {
      map_frame.window.location.href="about:blank";
    }
  else {
    //alert(map_frame.window.location.href);
    map_frame.window.location.href=u+'/map/get/'+nid;
    //alert(map_frame.window.location.href);
  }
}

function insert_map(u)
{
  var nid = document.getElementById('edit-node').value;
  if (nid == 0)
    {
      window.parent.close();
    }
  else {
    var macro=HTTPGet(u+'/map/macro/'+nid);
    // TODO: Error checking.
    insert_to_tinymce(macro, nid);
    window.parent.close();
  }
}

function insert_to_tinymce(text, nid)
{
  var image_url = window.parent.opener.tinyMCE.baseURL + '/plugins/drupalgmap/images/map.jpg';
  var content = '<img class="map" src="' + image_url + '" width="'+map_get_width(text)+'" height="'+map_get_height(text)+'" alt=\'[map|'+nid+'|'+map_get_width(text)+'|'+map_get_height(text)+']\' />';
  window.parent.opener.tinyMCE.execCommand("mceInsertContent", true, content);
}

// This function is responsible for popping up the maps editor.
function map_open_macro_editor(base_url)
{
  var macro = document.getElementById('edit-map').value;
  if (macro.length == 0)
    {
      window.open(base_url+'/map/plain/insert');
    }
  else {
    window.open(base_url+'/map/plain/update/'+macro);
  }
}
