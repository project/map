
Name: map.module
Author: OpenCraft <http://www.open-craft.com>
Maintainer: Mohammed Sameer <msameer@open-craft.com>
Dependencies:
1) GMap module 4.7 (Required)
2) Activeselect 4.7 (Optional)
3) TinyMCE 4.7 if you need to enable the tinymce plugin.

Description:
This module provides:
1) A new node type "map". Each map is a google mp.
2) A tinyMCE plugin, you can select any of the maps you created and insert it into any
node you want.
3) A filter to interpret the macro inserted bythe above plugin and generate a map.

Installation:
0) Install and configure the GMap module.
1) Copy the map/ directory into the modules/ directory.
2) Enable the "map module" from the drupal modules page.

Configuration:
1) Set the permissions in the access control page.
1) Configure an "input format" so that the map filter is activated. The map filter
must be below (more weight) than the HTML filter if you are going to use the HTML filter.

Installing the tinyMCE plugin:
1) Copy the drupalgmap/ directory to tinymce/tinymce/jscripts/tiny_mce/plugins/
2) Add the following php code to plugin_reg.php just before "return $plugins;"

$plugins['drupalgmap'] = array();
$plugins['drupalgmap']['theme_advanced_buttons1'] = array('drupalgmap');
$plugins['drupalgmap']['extended_valid_elements'] = array('div[class|width|height]');

3) Enable the plugin from the tinyMCE profiles settings page.
4) You must prevent tinymce from loading itself with the "map" text area.
In phptemplate based themes, I'd do override the "tinymce_theme" function in
template.php like this:

function phptemplate_tinymce_theme($limit, $textarea_name, $theme_name, $is_running) {
  if ($textarea_name == 'map')
    unset($limit);
  return $limit;
}
