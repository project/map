
/**
 * @file
 * This file will read a gmap macro and add the lines and markers to the map.
 *
 * Copyright OpenCraft |  http://www.open-craft.com
 * Maintainer: msameer@open-craft.com
 *
 * This software is distributed under the GNU General Public License.
 * Read the entire license text here: http://www.gnu.org/licenses/gpl.html
 */

// TODO: Lines are not drawn correctly ?
var macro;

addLoadEvent(adjust_map);

function map_store_macro(m)
{
  macro = m;
}

function adjust_map()
{
  var arr = macro.split("|");

  for (x in arr)
    {
      ar = arr[x].split('=');
      if (ar[0] == 'markers')
	{
	  add_markers(ar[1]);
	}
      else if (ar[0] == 'line1')
	{
	  add_line1(ar[1]);
	}
      else if (ar[0] == 'line2')
	{
	  add_line2(ar[1]);
	}
      else if (ar[0] == 'line3')
	{
	  add_line3(ar[1]);
	}

    }
  $('gmap-macrotext').value = map_to_macro(map);
}

function add_markers(text)
{
  var markers = text.split(' ');
  for (x in markers)
    {
      if (markers[x].length != 0)
	{
	  var splitstring=markers[x].split(",");
	  point = new GLatLng(splitstring[0],splitstring[1]);
	  map.addOverlay(marker=new GMarker(point));
	  map.drupal.pointsOverlays.push(marker);
	  map.drupal.points.push(point.lat() + ',' + point.lng());
	}
    }
}

// TODO: Combine.
function add_line1(text)
{
  var lines = text.split(' ');
  for (x in lines)
    {
      if (lines[x].length != 0)
	{
	  //alert(lines[x]);
	  var splitstring=lines[x].split(",");
	  point = new GLatLng(splitstring[0],splitstring[1]);

	  map.drupal.line1points.push(point);
	  if (map.drupal.line1overlay) map.removeOverlay(map.drupal.line1overlay);
	  map.drupal.line1overlay=new GPolyline(map.drupal.line1points, map.drupal.linecolors[0], 5);
	  map.addOverlay(map.drupal.line1overlay);
	  if (map.drupal.line1string.length > 0) map.drupal.line1string += ' + ';
	  map.drupal.line1string += point.lat() + ',' + point.lng();
	  map.drupal.gmapline1 = map.drupal.line1string;
	}
    }
}

function add_line2(text)
{
  var lines = text.split(' ');
  for (x in lines)
    {
      if (lines[x].length != 0)
	{
	  var splitstring=lines[x].split(",");
	  point = new GLatLng(splitstring[0],splitstring[1]);

          map.drupal.line2points.push(point);
          if (map.drupal.line2overlay) map.removeOverlay(map.drupal.line2overlay);
          map.drupal.line2overlay=new GPolyline(map.drupal.line2points, map.drupal.linecolors[1], 5);
          map.addOverlay(map.drupal.line2overlay);
          if (map.drupal.line2string.length > 0) map.drupal.line2string += ' + ';
          map.drupal.line2string += point.lat() + ',' + point.lng();
          map.drupal.gmapline2 = map.drupal.line2string;
	}
    }
}

function add_line3(text)
{
  var lines = text.split(' ');
  for (x in lines)
    {
      if (lines[x].length != 0)
	{
	  var splitstring=lines[x].split(",");
	  point = new GLatLng(splitstring[0],splitstring[1]);

	  map.drupal.line3points.push(point);
          if (map.drupal.line3overlay) map.removeOverlay(map.drupal.line3overlay);
          map.drupal.line3overlay=new GPolyline(map.drupal.line3points, map.drupal.linecolors[2], 5);
          map.addOverlay(map.drupal.line3overlay);
          if (map.drupal.line3string.length > 0) map.drupal.line3string += ' + ';
          map.drupal.line3string += point.lat() + ',' + point.lng();
          map.drupal.gmapline3 = map.drupal.line3string;
	}
    }
}
