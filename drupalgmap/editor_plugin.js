
/**
 * @file
 * TinyMCE plugin.
 *
 * Copyright OpenCraft |  http://www.open-craft.com
 * Maintainer: msameer@open-craft.com
 *
 * This software is distributed under the GNU General Public License.
 * Read the entire license text here: http://www.gnu.org/licenses/gpl.html
 */

function tinymce_map_get_width(t) {
  var arr=t.split('|');
  return arr[2];
}

function tinymce_map_get_height(t) {
  var arr=t.split('|');
  return arr[3];
}

/* Import plugin specific language pack */
tinyMCE.importPluginLanguagePack('drupalgmap', 'en');

var TinyMCE_DrupalGMapPlugin = {
  getInfo : function() {
    return {
      longname : 'DrupalGMap',
      author : 'Mohammed Sameer',
      authorurl : 'http://www.open-craft.com',
      infourl : 'http://drupal.org/project/map',
      version : tinyMCE.majorVersion + "." + tinyMCE.minorVersion
    };
  },

  getControlHTML : function(cn) {
    switch (cn) {
      case "drupalgmap": {
	// WE don't want the button if we are creating a map.
	if (document.getElementById('edit-map')) {
	  return "";
	}
	return tinyMCE.getButtonHTML(cn, 'lang_drupalgmap_desc', '{$pluginurl}/images/drupalgmap.jpg', 'mceDrupalGMap');
      }
    }
    return "";
  },

  execCommand : function(editor_id, element, command, user_interface, value) {
    // Handle commands
    switch (command) {
      case "mceDrupalGMap":
      var macro = "";
      var name = "";
      var nid = "", alt = "", captionTitle = "", captionDesc = "", link = "", align = "", width = "", height = "";
      var action = "insert";
      var template = new Array();
      var inst = tinyMCE.getInstanceById(editor_id);
      var focusElm = inst.getFocusElement();

      // get base url
      var base_url = tinyMCE.baseURL;
      base_url = base_url.substring(0, base_url.indexOf('modules'));
      template['width'] = 700;
      template['height'] = 650;
      template['html'] = false;

      template['file'] = base_url + 'index.php?q=map/selector/'+action+'/'+macro;

      //WARNING: "resizable : 'yes'" below is painfully important otherwise
      // tinymce will try to open a new window in IE using showModalDialog().
      // And for some reason showModalDialog() doesn't respect the target="_top"
      // attribute.
      tinyMCE.openWindow(template, {editor_id : editor_id, nid : nid, captionTitle : captionTitle, captionDesc : captionDesc, link : link, align : align, width : 800, height : 600, action : action,  scrollbars : 'yes'});
      return true;
    }

    // Pass to next handler in chain
    return false;
  },
  cleanup : function(type, content) {
    switch (type) {
      case "insert_to_editor_dom":
      break;
      case "get_from_editor_dom":
      break;
      case "insert_to_editor":
      // called when TinyMCE loads existing data or when updating code using
      // Edit HTML Source plugin
      // parse all the filters and replace them with placeholders.
      var startPos = 0;
      while ((startPos = content.indexOf('[map|', startPos)) != -1) {
	// Find end of object
	var endPos = content.indexOf(']', startPos);
	endPos++;
	// Insert
	var image_url = tinyMCE.baseURL + '/plugins/drupalgmap/images/map.jpg';
	var text = content.substring(startPos, endPos);
	var contentAfter = content.substring(endPos);
	content = content.substring(0, startPos);
	content += '<img class="map" src="' + image_url + '" width="' + tinymce_map_get_width(text) + '" height="' + tinymce_map_get_height(text)+'" alt=\''+text+'\' />';
	content += contentAfter;
	startPos = content.indexOf('>', startPos);
	startPos++;
      }
      break;

      case "get_from_editor": // called when TinyMCE exits or when the
      // Edit HTML Source plugin is clicked
      // Parse all placeholders and replace them with filters
      var startPos = -1;
      while ((startPos = content.indexOf('<img class="map"', startPos+1)) != -1) {
	var endPos = content.indexOf('/>', startPos);
	endPos += 2;

	var s = content.indexOf('[map|' , startPos, endPos);
	var e = content.indexOf(']' , startPos, endPos);
	e++;
	var contentBefore = content.substring(0, startPos);
	var contentAfter = content.substring(endPos);
	//alert(s);
	//alert(e);
	var text = content.substring(s, e);
	content = contentBefore + text + contentAfter;
	endpos = e+4;
	//alert(text);
      }
      break;
    }

    // Pass through to next handler in chain
    return content;
  }
};

tinyMCE.addPlugin("drupalgmap", TinyMCE_DrupalGMapPlugin);
