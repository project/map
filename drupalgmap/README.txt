drupalgmap plugin for TinyMCE
------------------------------
Written for Drupal 4.7 by Mohammed Sameer <msameer@open-craft.com> (based on the TinyMCE Flash plugin).

About:

   This plugin integrates the Drupal map module with TinyMCE allowing you
   to upload, browse and insert google maps into your posts.

Copyright:
Copyright (c) 2006 OpenCraft <http://www.open-craft.com>

License:
GPL.
